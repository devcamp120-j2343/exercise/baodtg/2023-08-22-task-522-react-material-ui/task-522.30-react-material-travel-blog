import ResponsiveAppBar from "./childComponents/NavbarMenu"
import HeaderSection from "./childComponents/HeaderSection"
import ProductSection from "./childComponents/ProductSection"
import Contact from "./childComponents/Contact"
import Footer from "./childComponents/Footer"

const TravelBlog = () => {
    return (

        < >
            <ResponsiveAppBar />
            <HeaderSection />
            <ProductSection />
            <Contact />
            <Footer />
        </>


    )
}
export default TravelBlog