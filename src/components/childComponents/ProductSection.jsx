import { Box, Container, Grid } from "@mui/material"
import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

import productImg1 from '../../assets/images/Iceland card image.png'
import productImg2 from '../../assets/images/Italy card image.png'
import productImg3 from '../../assets/images/Dubai card image.png'
import productImg4 from '../../assets/images/Patagonia Card Image.png'
const ProductSection = () => {
    const bull = (
        <Box
            component="span"
            sx={{ display: 'inline-block', mx: '2px', transform: 'scale(0.8)' }}
        >
            •
        </Box>
    );
    return (
        <>
            <Container maxWidth>
                <Grid container>
                    <Grid textAlign={'center'} md={12} item>
                        <h1 className="font-link" style={{ color: '#8A53FF' }}>JOIN OUR ADVENTURES</h1>
                    </Grid>
                    <Grid item md={12} textAlign={'center'}>
                        <h1 className="font-link" style={{ fontSize: '50px', margin: 0 }}>Discover the world with us</h1>
                    </Grid>

                    <Grid item md={12} textAlign={'center'}>
                        <Container maxWidth

                            sx={{
                                width: '90%',
                                my: 5,
                                mx: 'auto',
                                display: 'flex',
                                flexWrap: 'wrap',
                                justifyContent: 'space-between'
                            }}
                        >
                            <Card sx={{ minWidth: "345px" }}>
                                <CardMedia
                                    sx={{ height: 400 }}
                                    image={productImg1}
                                    title="Iceland"
                                />
                                <CardContent
                                    sx={{
                                        mt: 2,
                                        p: 0
                                    }}  >
                                    <Typography gutterBottom variant="h5" component="div">
                                        Iceland
                                    </Typography>

                                </CardContent>
                                <CardActions
                                    sx={{
                                        mt: 0,
                                        p: 0,
                                        justifyContent: 'center'
                                    }}
                                >
                                    <Button size="small">Discover</Button>

                                </CardActions>
                            </Card>
                        
                            <Card sx={{ minWidth: "345px" }}>
                                <CardMedia
                                    sx={{ height: 400 }}
                                    image={productImg2}
                                    title="Iceland"
                                />
                                <CardContent
                                    sx={{
                                        mt: 2,
                                        p: 0
                                    }}  >
                                    <Typography gutterBottom variant="h5" component="div">
                                    Italy
                                    </Typography>

                                </CardContent>
                                <CardActions
                                    sx={{
                                        mt: 0,
                                        p: 0,
                                        justifyContent: 'center'
                                    }}
                                >
                                    <Button size="small">Discover</Button>

                                </CardActions>
                            </Card>

                            <Card sx={{ minWidth: "345px" }}>
                                <CardMedia
                                    sx={{ height: 400 }}
                                    image={productImg3}
                                    title="Iceland"
                                />
                                <CardContent
                                    sx={{
                                        mt: 2,
                                        p: 0
                                    }}  >
                                    <Typography gutterBottom variant="h5" component="div">
                                    Dubai
                                    </Typography>

                                </CardContent>
                                <CardActions
                                    sx={{
                                        mt: 0,
                                        p: 0,
                                        justifyContent: 'center'
                                    }}
                                >
                                    <Button size="small">Discover</Button>

                                </CardActions>
                            </Card>

                            <Card sx={{ minWidth: "345px" }}>
                                <CardMedia
                                    sx={{ height: 400 }}
                                    image={productImg4}
                                    title="Iceland"
                                />
                                <CardContent
                                    sx={{
                                        mt: 2,
                                        p: 0
                                    }}  >
                                    <Typography gutterBottom variant="h5" component="div">
                                    Patagonia
                                    </Typography>

                                </CardContent>
                                <CardActions
                                    sx={{
                                        mt: 0,
                                        p: 0,
                                        justifyContent: 'center'
                                    }}
                                >
                                    <Button size="small">Discover</Button>

                                </CardActions>
                            </Card>
                        </Container>
                    </Grid>
                </Grid>
            </Container>
        </>
    )
}
export default ProductSection