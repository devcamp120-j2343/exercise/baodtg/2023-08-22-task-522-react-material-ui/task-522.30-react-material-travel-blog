import { Box, Button, Container, Grid, ThemeProvider, Typography, createTheme } from "@mui/material"
import * as React from 'react';

const HeaderSection = () => {
    const theme = createTheme();

    theme.typography.h3 = {
        fontSize: '40px',
        '@media (max-width:960px)': {
            fontSize: '40px',
        },
        [theme.breakpoints.up('lg')]: {
            fontSize: '80px',
        },
    };
    return (
        <Container maxWidth className="headerSection" style={{ paddingLeft: "0px" }}>

            <Grid container sx={{ height: '100%' }}>
                <Grid lg={5} sm={5} display={'flex'} alignItems={'center'} item sx={{ height: '100%' }}>
                    <Box sx={{ ml: 10 }}>
                        <ThemeProvider theme={theme}>
                            <Typography variant="h3"><span style={{ color: "#8A53FF" }}>Let’s talk</span> about your next trip!</Typography>
                        </ThemeProvider>
                        <p style={{ fontSize: '20px', margin: 0, marginTop: '30px' }}>Share your favorite travel destination and we will feature it in our next blog!</p>
                        <Button variant="Contained" sx={{ background: '#8A53FF', marginTop: '30px' }}>Share your story</Button>
                    </Box>
                </Grid>

            </Grid>



        </Container>
    )
}
export default HeaderSection