import { Box, Button, Container, FormControl, Grid, InputLabel, MenuItem, Select, TextField } from "@mui/material"

const Contact = () => {

    return (
        <>
            <Container maxWidth className="ContactSection">
                <Grid container>
                    <Grid textAlign={'center'} md={12} item>
                        <h1 className="font-link" style={{ color: '#8A53FF', marginTop: '90px' }}>Let’s build a community</h1>
                    </Grid>
                    <Grid item md={12} textAlign={'center'}>
                        <h1 className="font-link" style={{ fontSize: '50px', margin: 0 }}>Join our next destination</h1>
                    </Grid>
                    <Grid item md={12}>
                        <Box
                            sx={{
                                background: 'white',
                                width: '30%',
                                mx: 'auto',
                                px: 5,
                                py: 2,
                                mt: 2,
                                borderRadius: '15px'

                            }}>
                            <h3>Share your travels</h3>
                            <p>Suggest a new travel destination that you want to see and we will feature it in our blog.</p>
                            <TextField label="Destination name" variant="outlined"
                                sx={{
                                    width: '100%'
                                }} />

                            <FormControl fullWidth
                                sx={{
                                    mt: 1
                                }}>
                                <InputLabel id="demo-simple-select-label">Country of Destination</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    label="Country of Destination"
                                    sx={{
                                        width: '100%'
                                    }}
                                >
                                    <MenuItem value={10}>Ten</MenuItem>
                                    <MenuItem value={20}>Twenty</MenuItem>
                                    <MenuItem value={30}>Thirty</MenuItem>
                                </Select>
                            </FormControl>

                            <TextField label="Your name" variant="outlined"
                                sx={{
                                    width: '100%',
                                    mt: 1
                                }} />

                            <FormControl fullWidth
                                sx={{
                                    mt: 1
                                }}>
                                <InputLabel id="demo-simple-select-label">Shooting category</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    label="Country of Destination"
                                    sx={{
                                        width: '100%'
                                    }}
                                >
                                    <MenuItem value={10}>Ten</MenuItem>
                                    <MenuItem value={20}>Twenty</MenuItem>
                                    <MenuItem value={30}>Thirty</MenuItem>
                                </Select>

                                <TextField label="Describe what kind of photos you want to focus on" variant="outlined"

                                    sx={{
                                        width: '100%',
                                        mt: 1,
                                        "& .MuiInputBase-root": {
                                            height: 80
                                        },
                                    }}

                                />

                                <Button variant="contained" sx={{ background: "#8A53FF", width: '35%', mt: 1 }}>Submit</Button>
                            </FormControl>
                        </Box>
                    </Grid>
                </Grid>
            </Container>

        </>
    )
}
export default Contact